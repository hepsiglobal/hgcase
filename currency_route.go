package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"

	"bitbucket.org/semihsari/hgcase/datastore"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/cors"
)

type currencyRoute struct {
	dataStore datastore.IDataStore
}

type CurrencyResponse struct {
	Success bool                `json:"success"`
	Error   string              `json:"error"`
	Result  *datastore.Currency `json:"result"`
}

func (rs currencyRoute) Routes() chi.Router {
	r := chi.NewRouter()
	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Content-Type"},
		AllowCredentials: false,
	}))
	r.Get("/currency/{currency}", rs.Get)
	r.Get("/coverage", rs.Coverage)
	return r
}

func (rs currencyRoute) Coverage(w http.ResponseWriter, r *http.Request) {
	file, err := os.Open("./coverage.html")
	if err != nil {
		w.WriteHeader(404)
		w.Write([]byte("NOT FOUND"))
		return
	}
	defer file.Close()

	body, _ := ioutil.ReadAll(file)
	w.WriteHeader(http.StatusOK)
	w.Write(body)
}

func (rs currencyRoute) Get(w http.ResponseWriter, r *http.Request) {
	currency := chi.URLParam(r, "currency")
	currencyInfo, err := rs.dataStore.Get(currency)
	if err != nil {
		w.WriteHeader(404)
		response := &CurrencyResponse{
			Success: false,
			Error:   err.Error(),
		}

		json.NewEncoder(w).Encode(response)
		return
	}

	w.WriteHeader(http.StatusOK)
	response := &CurrencyResponse{
		Success: true,
		Error:   "",
		Result:  currencyInfo,
	}
	json.NewEncoder(w).Encode(response)
}
