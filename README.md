# hgcase: A simple rest api service for Simple Currency Converter Application

hgcase gives the most appropriate currency conversion data from service providers.
It stores data in-memory

## Compiling

### With Docker

- go to project folder ( cd /path/to/hgcase )
- docker build -t hgcase .
- docker run -p 127.0.0.1:2626:2626/tcp hgcase
- go to http://127.0.0.1:2626/currency/USD and check
- go to http://127.0.0.1:2626/coverage and check test coverage

## Deployment

### Pipeline

IF you want use pipeline for build or deploy edit bitbucket-pipelines.yml or ...gitlab-ci.yml

## Features
        
- Get Currency
    - GET http://127.0.0.1:2626/currency/{currency}
      
        - Response Body
        ```json
        {
            "success": true,
            "error": "",
            "result": {
                "data": 8.5,
                "currency": "EUR"
            }
        }
        ```
            Response Notes: 
              - result.data is required (float64)
              - result.currency a string and required
              - if success is true error is empty, check result
              - if success is false result is empty, check error message
