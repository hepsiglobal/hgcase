package client

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

var (
	ErrResponseBodyReading = fmt.Errorf("response body reading")
	ErrResponseBodyClosing = fmt.Errorf("response body closing")
	ErrHttpNewRequest      = fmt.Errorf("http new request failed")
	ErrRequestSend         = fmt.Errorf("request sending failed")
)

type HTTPClient interface {
	Do(*http.Request) (*http.Response, error)
}

type Reader interface {
	ReadAll(closer io.ReadCloser) ([]byte, error)
	Close(closer io.ReadCloser) error
}

type read struct {
}

func NewReader() *read {
	return &read{}
}

func (r *read) ReadAll(closer io.ReadCloser) ([]byte, error) {
	return ioutil.ReadAll(closer)
}

func (r *read) Close(closer io.ReadCloser) error {
	return closer.Close()
}

type Getter interface {
	Get(url string) ([]byte, error)
}

type getter struct {
	client HTTPClient
	reader Reader
}

func NewGetter(client HTTPClient, reader Reader) *getter {
	return &getter{
		client: client,
		reader: reader,
	}
}

func (c *getter) Get(url string) ([]byte, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, ErrHttpNewRequest
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return nil, ErrRequestSend
	}

	body, err := c.reader.ReadAll(resp.Body)
	if err != nil {
		return nil, ErrResponseBodyReading
	}

	if err := c.reader.Close(resp.Body); err != nil {
		return nil, ErrResponseBodyClosing
	}

	return body, nil
}
