package client

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/mock"

	"github.com/stretchr/testify/assert"
)

type HTTPClientMock struct {
	DoFunc func(*http.Request) (*http.Response, error)
}

func (H HTTPClientMock) Do(r *http.Request) (*http.Response, error) {
	return H.DoFunc(r)
}

func TestGetterGet_WithSuccess(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte(`OK`))
	}))
	defer server.Close()

	mr := &MockReader{}
	mr.On("ReadAll", mock.Anything).Return([]byte("OK"), nil)
	mr.On("Close", mock.Anything).Return(nil)

	getter := NewGetter(server.Client(), mr)
	get, err := getter.Get("http://example.com")
	assert.Nil(t, err)
	assert.Equal(t, get, []byte("OK"))
}

func TestGetterGet_WithErrResponseBodyReading(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte(`OK`))
	}))
	defer server.Close()

	mr := &MockReader{}
	mr.On("ReadAll", mock.Anything).Return(nil, fmt.Errorf("make error"))
	mr.On("Close", mock.Anything).Return(nil)

	getter := NewGetter(server.Client(), mr)
	get, err := getter.Get("http://example.com")
	assert.Nil(t, get)
	assert.NotNil(t, err)
	assert.Equal(t, err, ErrResponseBodyReading)
}

func TestGetterGet_WithErrResponseBodyClosing(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte(`OK`))
	}))
	defer server.Close()

	mr := &MockReader{}
	mr.On("ReadAll", mock.Anything).Return([]byte("OK"), nil)
	mr.On("Close", mock.Anything).Return(fmt.Errorf("make error"))

	getter := NewGetter(server.Client(), mr)
	get, err := getter.Get("http://example.com")
	assert.Nil(t, get)
	assert.NotNil(t, err)
	assert.Equal(t, err, ErrResponseBodyClosing)
}

func TestGetterGet_WithErrRequestSend(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte(""))
	}))
	defer server.Close()

	mr := &MockReader{}
	mr.On("ReadAll", mock.Anything).Return([]byte("OK"), nil)
	mr.On("Close", mock.Anything).Return(nil)

	getter := NewGetter(&HTTPClientMock{
		DoFunc: func(request *http.Request) (response *http.Response, e error) {
			return nil, fmt.Errorf("make error")
		},
	}, mr)
	get, err := getter.Get("http://example.com")
	assert.Nil(t, get)
	assert.NotNil(t, err)
	assert.Equal(t, err, ErrRequestSend)
}

func TestGetterGet_WithErrHttpNewRequest(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte(""))
	}))
	defer server.Close()

	mr := &MockReader{}
	mr.On("ReadAll", mock.Anything).Return([]byte("OK"), nil)
	mr.On("Close", mock.Anything).Return(nil)

	getter := NewGetter(server.Client(), mr)
	get, err := getter.Get("%vendpoint")
	assert.Nil(t, get)
	assert.NotNil(t, err)
	assert.Equal(t, err, ErrHttpNewRequest)
}
