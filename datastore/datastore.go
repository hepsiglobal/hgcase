package datastore

import (
	"fmt"
	"sync"
)

var (
	ErrCurrencyNotFound = fmt.Errorf("currency not found")
)

type Currency struct {
	Data     float64 `json:"data"`
	Currency string  `json:"currency"`
}

type IDataStore interface {
	Get(currency string) (currencyInfo *Currency, err error)
	Update(currencies []*Currency)
}

type dataStore struct {
	sync.RWMutex
	currencies []*Currency
}

func NewDataStore(currencies []*Currency) *dataStore {
	return &dataStore{
		currencies: currencies,
	}
}

func (ds *dataStore) Get(currency string) (currencyInfo *Currency, err error) {
	ds.RLock()
	defer ds.RUnlock()
	for _, c := range ds.currencies {
		if c.Currency == currency {
			return c, nil
		}
	}
	return nil, ErrCurrencyNotFound
}

func (ds *dataStore) Update(currencies []*Currency) {
	ds.Lock()
	defer ds.Unlock()
	ds.currencies = currencies
}
