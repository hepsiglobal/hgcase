package datastore

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_NewDataStore(t *testing.T) {
	type args struct {
		currencies []*Currency
	}
	tests := []struct {
		name string
		args args
		want IDataStore
	}{
		{
			name: "return storer",
			args: args{},
			want: &dataStore{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewDataStore(tt.args.currencies); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewDataStore expected %v, actual %v", tt.want, got)
			}
		})
	}
}

func TestDataStoreGet_WithSuccess(t *testing.T) {
	type fields struct {
		data []*Currency
	}
	tests := []struct {
		name   string
		fields fields
		want   *Currency
	}{
		{
			name: "return get currency",
			fields: fields{
				data: []*Currency{
					{15.2422, "EUR"},
				},
			},
			want: &Currency{15.2422, "EUR"},
		},
		{
			name: "return get currency",
			fields: fields{
				data: []*Currency{
					{15.2422, "EUR"},
					{13.2422, "USD"},
					{17.2422, "GBP"},
				},
			},
			want: &Currency{15.2422, "EUR"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &dataStore{
				currencies: tt.fields.data,
			}
			got, err := s.Get("EUR")
			assert.Nil(t, err)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("dataStore.Get(EUR) expected %v, actual %v", tt.want, got)
			}

		})
	}
}

func TestDataStoreGet_WithNotFound(t *testing.T) {
	s := &dataStore{
		currencies: []*Currency{
			{15.2422, "EUR"},
			{17.2422, "GBP"},
		},
	}
	got, err := s.Get("USD")
	assert.NotNil(t, err)
	assert.Equal(t, err, ErrCurrencyNotFound)
	assert.Nil(t, got)
}

func TestDataStore_Update(t *testing.T) {
	s := &dataStore{}
	s.Update([]*Currency{
		{15.2422, "EUR"},
		{17.2422, "GBP"},
	})

	got, err := s.Get("EUR")
	want := &Currency{Data: 15.2422, Currency: "EUR"}
	assert.Nil(t, err)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("dataStore.Get(EUR) expected %v, actual %v", want, got)
	}

	got, err = s.Get("GBP")
	want = &Currency{Data: 17.2422, Currency: "GBP"}
	assert.Nil(t, err)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("dataStore.Get(GBP) expected %v, actual %v", want, got)
	}
}
