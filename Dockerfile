FROM golang:1.17 as builder

WORKDIR /app
COPY . .
RUN go mod download
RUN go install github.com/vektra/mockery/v2/
RUN mockery --case "underscore" --dir ./client --name "Reader" --inpackage
RUN mockery --case "underscore" --dir ./client --name "Getter" --inpackage
RUN mockery --case "underscore" --dir ./datastore --name "IDataStore" --inpackage
RUN mockery --case "underscore" --dir ./provider --name "Provider" --inpackage
RUN go test ./... -coverprofile cover.out
RUN go tool cover -html=cover.out -o coverage.html
RUN CGO_ENABLED=0 go build -o hgcase

FROM alpine:3.10
COPY --from=builder /app/hgcase /
COPY --from=builder /app/coverage.html /
EXPOSE 2626
CMD ["/hgcase"]