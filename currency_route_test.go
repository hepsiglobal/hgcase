package main

import (
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/semihsari/hgcase/datastore"

	"github.com/go-chi/chi/v5"
)

func TestRouteGetWithSuccess(t *testing.T) {
	r := chi.NewRouter()
	ds := datastore.NewDataStore([]*datastore.Currency{
		{
			Currency: "EUR",
			Data:     15.4353,
		},
	})
	r.Mount("/", currencyRoute{ds}.Routes())
	ts := httptest.NewServer(r)
	defer ts.Close()

	expected := `{"success":true,"error":"","result":{"data":15.4353,"currency":"EUR"}}`
	or, resp := testRequest(t, ts, "GET", "/currency/EUR", nil)
	if resp != expected {
		t.Fatalf("expected %s actual %s", expected, resp)
	}
	if or.StatusCode != http.StatusOK {
		t.Fatalf("status code not ok %d", or.StatusCode)
	}
}

func TestRouteGetWithNotFound(t *testing.T) {
	r := chi.NewRouter()
	ds := datastore.NewDataStore([]*datastore.Currency{
		{
			Currency: "EUR",
			Data:     15.4353,
		},
	})
	r.Mount("/", currencyRoute{ds}.Routes())
	ts := httptest.NewServer(r)
	defer ts.Close()

	expected := `{"success":false,"error":"currency not found","result":null}`
	or, resp := testRequest(t, ts, "GET", "/currency/GBP", nil)
	if resp != expected {
		t.Fatalf("expected %s actual %s", expected, resp)
	}

	if or.StatusCode != http.StatusNotFound {
		t.Fatalf("status code expected 404 actual %d", or.StatusCode)
	}
}

func testRequest(t *testing.T, ts *httptest.Server, method, path string, body io.Reader) (*http.Response, string) {
	req, err := http.NewRequest(method, ts.URL+path, body)
	if err != nil {
		t.Fatal(err)
		return nil, ""
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
		return nil, ""
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
		return nil, ""
	}
	defer resp.Body.Close()

	return resp, strings.TrimRight(string(respBody), "\r\n")
}
