package provider

import (
	"fmt"
	"reflect"
	"testing"

	"bitbucket.org/semihsari/hgcase/client"

	"github.com/stretchr/testify/assert"
)

func TestProvider1_Name(t *testing.T) {
	getter := &client.MockGetter{}

	provider1 := NewProvider1(getter, "http://provider-url")
	name := provider1.Name()
	assert.Equal(t, name, "Provider 1")
}

func TestProvider1_Get_WithSuccess(t *testing.T) {
	getter := &client.MockGetter{}
	getter.On("Get", "http://provider-url").Return([]byte(`
{
  "EUR": 11.4,
  "USD": 9.96,
  "GBP": 13.31
}`), nil)

	provider1 := NewProvider1(getter, "http://provider-url")
	moneys, err := provider1.Get()
	want := []*Money{
		{
			Currency: USD,
			Data:     9.96,
		},
		{
			Currency: EUR,
			Data:     11.4,
		},
		{
			Currency: GBP,
			Data:     13.31,
		},
	}
	assert.Nil(t, err)
	if !reflect.DeepEqual(moneys, want) {
		t.Errorf("moneys expected %v, actual %v", want, moneys)
	}
}

func TestProvider1_Get_WithErrGetterGet(t *testing.T) {
	getter := &client.MockGetter{}
	getter.On("Get", "http://provider-url").Return(nil, fmt.Errorf("make error"))

	provider1 := NewProvider1(getter, "http://provider-url")
	moneys, err := provider1.Get()
	assert.Nil(t, moneys)
	assert.NotNil(t, err)
	assert.Equal(t, err, ErrGetterGet)
}

func TestProvider1_Get_WithErrJsonUnmarshal(t *testing.T) {
	getter := &client.MockGetter{}
	getter.On("Get", "http://provider-url").Return([]byte(`
{
  "EUR": 11.4, // invalid JSON
  "USD": 9.96,
  "GBP": 13.31
}`), nil)

	provider1 := NewProvider1(getter, "http://provider-url")
	moneys, err := provider1.Get()
	assert.Nil(t, moneys)
	assert.NotNil(t, err)
	assert.Equal(t, err, ErrJsonUnmarshal)
}
