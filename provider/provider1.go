package provider

import (
	"encoding/json"
	"log"

	"bitbucket.org/semihsari/hgcase/client"
)

type provider1Response struct {
	Eur float64 `json:"EUR"`
	Usd float64 `json:"USD"`
	Gbp float64 `json:"GBP"`
}

type provider1 struct {
	url    string
	getter client.Getter
}

func NewProvider1(getter client.Getter, url string) *provider1 {
	return &provider1{
		url:    url,
		getter: getter,
	}
}

func (p *provider1) Name() string {
	return "Provider 1"
}

func (p *provider1) Get() ([]*Money, error) {
	response, err := p.getter.Get(p.url)
	if err != nil {
		return nil, ErrGetterGet
	}

	p1Response := provider1Response{}
	if err := json.Unmarshal(response, &p1Response); err != nil {
		log.Printf("provider 1 response %s %s\n", p.url, string(response))
		return nil, ErrJsonUnmarshal
	}

	currencies := []*Money{
		{
			Currency: USD,
			Data:     p1Response.Usd,
		},
		{
			Currency: EUR,
			Data:     p1Response.Eur,
		},
		{
			Currency: GBP,
			Data:     p1Response.Gbp,
		},
	}

	return currencies, nil
}
