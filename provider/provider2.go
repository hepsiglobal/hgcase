package provider

import (
	"encoding/json"
	"log"

	"bitbucket.org/semihsari/hgcase/client"
)

type provider2Response struct {
	Eur float64 `json:"EUR"`
	Usd float64 `json:"USD"`
	Gbp float64 `json:"GBP"`
}

type provider2 struct {
	url    string
	getter client.Getter
}

func NewProvider2(getter client.Getter, url string) *provider2 {
	return &provider2{
		url:    url,
		getter: getter,
	}
}

func (p *provider2) Name() string {
	return "Provider 2"
}

func (p *provider2) Get() ([]*Money, error) {
	response, err := p.getter.Get(p.url)
	if err != nil {
		return nil, ErrGetterGet
	}

	p2Response := provider2Response{}
	if err := json.Unmarshal(response, &p2Response); err != nil {
		log.Printf("provider 2 response %s\n", string(response))
		return nil, ErrJsonUnmarshal
	}

	currencies := []*Money{
		{
			Currency: USD,
			Data:     p2Response.Usd,
		},
		{
			Currency: EUR,
			Data:     p2Response.Eur,
		},
		{
			Currency: GBP,
			Data:     p2Response.Gbp,
		},
	}

	return currencies, nil
}
