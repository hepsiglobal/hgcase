package provider

import (
	"fmt"
	"reflect"
	"testing"

	"bitbucket.org/semihsari/hgcase/client"

	"github.com/stretchr/testify/assert"
)

func TestProvider2_Name(t *testing.T) {
	getter := &client.MockGetter{}

	provider2 := NewProvider2(getter, "http://provider-url")
	name := provider2.Name()
	assert.Equal(t, name, "Provider 2")
}

func TestProvider2_Get_WithSuccess(t *testing.T) {
	getter := &client.MockGetter{}
	getter.On("Get", "http://provider-url").Return([]byte(`
{
  "EUR": 11.4,
  "USD": 9.96,
  "GBP": 13.31
}`), nil)

	provider2 := NewProvider2(getter, "http://provider-url")
	moneys, err := provider2.Get()
	want := []*Money{
		{
			Currency: USD,
			Data:     9.96,
		},
		{
			Currency: EUR,
			Data:     11.4,
		},
		{
			Currency: GBP,
			Data:     13.31,
		},
	}
	assert.Nil(t, err)
	if !reflect.DeepEqual(moneys, want) {
		t.Errorf("moneys expected %v, actual %v", want, moneys)
	}
}

func TestProvider2_Get_WithErrGetterGet(t *testing.T) {
	getter := &client.MockGetter{}
	getter.On("Get", "http://provider-url").Return(nil, fmt.Errorf("make error"))

	provider2 := NewProvider2(getter, "http://provider-url")
	moneys, err := provider2.Get()
	assert.Nil(t, moneys)
	assert.NotNil(t, err)
	assert.Equal(t, err, ErrGetterGet)
}

func TestProvider2_Get_WithErrJsonUnmarshal(t *testing.T) {
	getter := &client.MockGetter{}
	getter.On("Get", "http://provider-url").Return([]byte(`
{
  "EUR": 11.4, // invalid JSON
  "USD": 9.96,
  "GBP": 13.31
}`), nil)

	provider2 := NewProvider2(getter, "http://provider-url")
	moneys, err := provider2.Get()
	assert.Nil(t, moneys)
	assert.NotNil(t, err)
	assert.Equal(t, err, ErrJsonUnmarshal)
}
