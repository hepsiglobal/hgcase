package provider

import (
	"encoding/json"
	"log"

	"bitbucket.org/semihsari/hgcase/client"
)

type provider3Response struct {
	Eur float64 `json:"EUR"`
	Usd float64 `json:"USD"`
	Gbp float64 `json:"GBP"`
}

type provider3 struct {
	url    string
	getter client.Getter
}

func NewProvider3(getter client.Getter, url string) *provider3 {
	return &provider3{
		url:    url,
		getter: getter,
	}
}

func (p *provider3) Name() string {
	return "Provider 3"
}

func (p *provider3) Get() ([]*Money, error) {
	response, err := p.getter.Get(p.url)
	if err != nil {
		return nil, ErrGetterGet
	}

	p3Response := provider3Response{}
	if err := json.Unmarshal(response, &p3Response); err != nil {
		log.Printf("provider 3 response %s\n", string(response))
		return nil, ErrJsonUnmarshal
	}

	currencies := []*Money{
		{
			Currency: USD,
			Data:     p3Response.Usd,
		},
		{
			Currency: EUR,
			Data:     p3Response.Eur,
		},
		{
			Currency: GBP,
			Data:     p3Response.Gbp,
		},
	}

	return currencies, nil
}
