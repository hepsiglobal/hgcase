package provider

import (
	"encoding/xml"
	"log"
	"strconv"

	"bitbucket.org/semihsari/hgcase/client"
)

type tcmbResponse struct {
	XMLName  xml.Name `xml:"Tarih_Date"`
	Text     string   `xml:",chardata"`
	Tarih    string   `xml:"Tarih,attr"`
	Date     string   `xml:"Date,attr"`
	BultenNo string   `xml:"Bulten_No,attr"`
	Currency []struct {
		Text            string `xml:",chardata"`
		CrossOrder      string `xml:"CrossOrder,attr"`
		Kod             string `xml:"Kod,attr"`
		CurrencyCode    string `xml:"CurrencyCode,attr"`
		Unit            string `xml:"Unit"`
		Isim            string `xml:"Isim"`
		CurrencyName    string `xml:"CurrencyName"`
		ForexBuying     string `xml:"ForexBuying"`
		ForexSelling    string `xml:"ForexSelling"`
		BanknoteBuying  string `xml:"BanknoteBuying"`
		BanknoteSelling string `xml:"BanknoteSelling"`
		CrossRateUSD    string `xml:"CrossRateUSD"`
		CrossRateOther  string `xml:"CrossRateOther"`
	} `xml:"Currency"`
}

type tcmb struct {
	url    string
	getter client.Getter
}

func NewTcmb(getter client.Getter, url string) *tcmb {
	return &tcmb{
		url:    url,
		getter: getter,
	}
}

func (p *tcmb) Name() string {
	return "Merkez Bankası"
}

func (p *tcmb) Get() ([]*Money, error) {
	response, err := p.getter.Get(p.url)
	if err != nil {
		return nil, ErrGetterGet
	}

	tResponse := tcmbResponse{}
	if err := xml.Unmarshal(response, &tResponse); err != nil {
		log.Printf("tcmb response %s\n", string(response))
		return nil, ErrXmlUnmarshal
	}

	var currencies []*Money
	for _, tr := range tResponse.Currency {
		if tr.CurrencyCode == "USD" || tr.CurrencyCode == "EUR" || tr.CurrencyCode == "GBP" {
			amount, err := strconv.ParseFloat(tr.ForexSelling, 64)
			if err != nil {
				return nil, ErrCurrencyAmountParsingFailed
			}
			currencies = append(currencies, &Money{
				Data:     amount,
				Currency: Currency(tr.CurrencyCode),
			})
		}
	}

	return currencies, nil
}
