package provider

import (
	"fmt"
	"reflect"
	"testing"

	"bitbucket.org/semihsari/hgcase/client"

	"github.com/stretchr/testify/assert"
)

func TestTcmb_Name(t *testing.T) {
	getter := &client.MockGetter{}

	tcmb := NewTcmb(getter, "http://provider-url")
	name := tcmb.Name()
	assert.Equal(t, name, "Merkez Bankası")
}

func TestTcmb_Get_WithSuccess(t *testing.T) {
	getter := &client.MockGetter{}
	getter.On("Get", "http://provider-url").Return([]byte(`<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="isokur.xsl"?>
<Tarih_Date Tarih="03.12.2021" Date="12/03/2021"  Bulten_No="2021/226" >
	<Currency CrossOrder="0" Kod="USD" CurrencyCode="USD">
			<Unit>1</Unit>
			<Isim>ABD DOLARI</Isim>
			<CurrencyName>US DOLLAR</CurrencyName>
			<ForexBuying>13.6801</ForexBuying>
			<ForexSelling>13.7048</ForexSelling>
			<BanknoteBuying>13.6705</BanknoteBuying>
			<BanknoteSelling>13.7253</BanknoteSelling>
			<CrossRateUSD/>
			<CrossRateOther/>
	</Currency>
	<Currency CrossOrder="9" Kod="EUR" CurrencyCode="EUR">
			<Unit>1</Unit>
			<Isim>EURO</Isim>
			<CurrencyName>EURO</CurrencyName>
			<ForexBuying>15.4548</ForexBuying>
			<ForexSelling>15.4826</ForexSelling>
			<BanknoteBuying>15.4439</BanknoteBuying>
			<BanknoteSelling>15.5058</BanknoteSelling>
				<CrossRateUSD/>
				<CrossRateOther>1.1297</CrossRateOther>
	</Currency>
    <Currency CrossOrder="10" Kod="GBP" CurrencyCode="GBP">
			<Unit>1</Unit>
			<Isim>İNGİLİZ STERLİNİ</Isim>
			<CurrencyName>POUND STERLING</CurrencyName>
			<ForexBuying>18.1352</ForexBuying>
			<ForexSelling>18.2298</ForexSelling>
			<BanknoteBuying>18.1226</BanknoteBuying>
			<BanknoteSelling>18.2571</BanknoteSelling>
				<CrossRateUSD/>
				<CrossRateOther>1.3279</CrossRateOther>
	</Currency>
</Tarih_Date>`), nil)

	tcmb := NewTcmb(getter, "http://provider-url")
	moneys, err := tcmb.Get()
	want := []*Money{
		{
			Currency: USD,
			Data:     13.7048,
		},
		{
			Currency: EUR,
			Data:     15.4826,
		},
		{
			Currency: GBP,
			Data:     18.2298,
		},
	}
	assert.Nil(t, err)
	if !reflect.DeepEqual(moneys, want) {
		t.Errorf("moneys expected %v, actual %v", want, moneys)
	}
}

func TestTcmb_Get_WithErrGetterGet(t *testing.T) {
	getter := &client.MockGetter{}
	getter.On("Get", "http://provider-url").Return(nil, fmt.Errorf("make error"))

	tcmb := NewTcmb(getter, "http://provider-url")
	moneys, err := tcmb.Get()
	assert.Nil(t, moneys)
	assert.NotNil(t, err)
	assert.Equal(t, err, ErrGetterGet)
}

func TestTcmb_Get_WithErrXmlUnmarshal(t *testing.T) {
	getter := &client.MockGetter{}
	getter.On("Get", "http://provider-url").Return([]byte(`<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="isokur.xsl"?>
<Tarih_Date Tarih="03.12.2021" Date="12/03/2021"  Bulten_No="2021/226" >
	<Currency CrossOrder="0" Kod="USD" CurrencyCode="USD">
			<Unit>1</Unit>
			<Isim>ABD DOLARI</Isim>

			INVALID XML

</Tarih_Date>`), nil)

	tcmb := NewTcmb(getter, "http://provider-url")
	moneys, err := tcmb.Get()
	assert.Nil(t, moneys)
	assert.NotNil(t, err)
	assert.Equal(t, err, ErrXmlUnmarshal)
}

func TestTcmb_Get_WithErrCurrencyAmountParsingFailed(t *testing.T) {
	getter := &client.MockGetter{}
	getter.On("Get", "http://provider-url").Return([]byte(`<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="isokur.xsl"?>
<Tarih_Date Tarih="03.12.2021" Date="12/03/2021"  Bulten_No="2021/226" >
	<Currency CrossOrder="0" Kod="USD" CurrencyCode="USD">
			<Unit>1</Unit>
			<Isim>ABD DOLARI</Isim>
			<CurrencyName>US DOLLAR</CurrencyName>
			<ForexBuying>13.6801</ForexBuying>
			<ForexSelling>invalid float value</ForexSelling>
			<BanknoteBuying>13.6705</BanknoteBuying>
			<BanknoteSelling>13.7253</BanknoteSelling>
			<CrossRateUSD/>
			<CrossRateOther/>
	</Currency>
</Tarih_Date>`), nil)

	tcmb := NewTcmb(getter, "http://provider-url")
	moneys, err := tcmb.Get()
	assert.Nil(t, moneys)
	assert.NotNil(t, err)
	assert.Equal(t, err, ErrCurrencyAmountParsingFailed)
}
