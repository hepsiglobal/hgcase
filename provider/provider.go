package provider

import (
	"fmt"
)

type Currency string

const (
	USD Currency = "USD"
	EUR Currency = "EUR"
	GBP Currency = "GBP"
)

var (
	ErrGetterGet                   = fmt.Errorf("get request failed")
	ErrJsonUnmarshal               = fmt.Errorf("json unmarshaling failed")
	ErrXmlUnmarshal                = fmt.Errorf("xml unmarshaling failed")
	ErrCurrencyAmountParsingFailed = fmt.Errorf("currency amount parsing failed")
)

type Money struct {
	Data     float64
	Currency Currency
}

type Provider interface {
	Get() ([]*Money, error)
	Name() string
}
