module bitbucket.org/semihsari/hgcase

go 1.15

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/go-chi/cors v1.2.0
	github.com/kr/pretty v0.1.0
	github.com/stretchr/testify v1.7.0
	github.com/vektra/mockery/v2 v2.9.4 // indirect
	golang.org/x/tools v0.1.8 // indirect
)
