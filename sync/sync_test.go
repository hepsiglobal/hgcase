package sync

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"bitbucket.org/semihsari/hgcase/provider"

	"bitbucket.org/semihsari/hgcase/datastore"
)

func TestPull_WithSuccess(t *testing.T) {
	ds := datastore.NewDataStore(nil)

	p1 := &provider.MockProvider{}
	p1.On("Name").Return("Provider 1")
	p1.On("Get").Return([]*provider.Money{
		{
			Currency: "EUR",
			Data:     99,
		},
		{
			Currency: "USD",
			Data:     77,
		},
		{
			Currency: "GBP",
			Data:     11,
		},
	}, nil)

	p2 := &provider.MockProvider{}
	p2.On("Name").Return("Provider 2")
	p2.On("Get").Return([]*provider.Money{
		{
			Currency: "EUR",
			Data:     33,
		},
		{
			Currency: "USD",
			Data:     44,
		},
		{
			Currency: "GBP",
			Data:     88,
		},
	}, nil)

	p3 := &provider.MockProvider{}
	p3.On("Name").Return("Provider 3")
	p3.On("Get").Return([]*provider.Money{
		{
			Currency: "EUR",
			Data:     66,
		},
		{
			Currency: "USD",
			Data:     22,
		},
		{
			Currency: "GBP",
			Data:     55,
		},
	}, nil)

	p4 := &provider.MockProvider{}
	p4.On("Name").Return("Provider 4")
	p4.On("Get").Return(nil, fmt.Errorf("make error"))

	syncer := NewSync(ds, []provider.Provider{
		p3, p4, p2, p1,
	})
	syncer.Pull()

	eur, err := ds.Get("EUR")
	assert.Nil(t, err)
	assert.Equal(t, eur.Data, float64(33))

	usd, err := ds.Get("USD")
	assert.Nil(t, err)
	assert.Equal(t, usd.Data, float64(22))

	gbp, err := ds.Get("GBP")
	assert.Nil(t, err)
	assert.Equal(t, gbp.Data, float64(11))
}
