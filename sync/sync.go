package sync

import (
	"log"
	sync2 "sync"
	"time"

	"bitbucket.org/semihsari/hgcase/datastore"
	"bitbucket.org/semihsari/hgcase/provider"
)

type ISync interface {
	StartPuller()
}

type sync struct {
	dataStore datastore.IDataStore
	providers []provider.Provider
	moneys    []*provider.Money
}

func NewSync(dataStore datastore.IDataStore, providers []provider.Provider) *sync {
	return &sync{
		dataStore: dataStore,
		providers: providers,
	}
}

func (s *sync) StartPuller() {
	for {
		s.Pull()
		time.Sleep(time.Minute * 10)
	}
}

func (s *sync) Pull() {
	wg := sync2.WaitGroup{}

	s.moneys = nil
	for _, p := range s.providers {
		log.Printf("%s request starting", p.Name())
		wg.Add(1)
		go func(p provider.Provider) {
			defer wg.Done()
			curr, err := p.Get()
			if err != nil {
				log.Printf("%s request failed %v", p.Name(), err)
				return
			}
			s.moneys = append(s.moneys, curr...)

			log.Printf("%s request finished", p.Name())
		}(p)
	}
	wg.Wait()

	mMap := make(map[provider.Currency]float64)
	for _, m := range s.moneys {
		if mMap[m.Currency] == 0 || m.Data < mMap[m.Currency] {
			mMap[m.Currency] = m.Data
		}
	}

	currencies := make([]*datastore.Currency, len(mMap))
	var i int
	for c, m := range mMap {
		currencies[i] = &datastore.Currency{
			Data:     m,
			Currency: string(c),
		}
		i++
	}
	s.dataStore.Update(currencies)
}
