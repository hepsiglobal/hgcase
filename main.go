package main

import (
	"log"
	"net/http"
	"time"

	"bitbucket.org/semihsari/hgcase/sync"

	"bitbucket.org/semihsari/hgcase/client"

	"bitbucket.org/semihsari/hgcase/provider"

	"bitbucket.org/semihsari/hgcase/datastore"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func main() {
	httpGetter := client.NewGetter(&http.Client{Timeout: time.Second * 5}, client.NewReader())
	dataStore := datastore.NewDataStore(nil)
	p1 := provider.NewProvider1(httpGetter, "https://run.mocky.io/v3/f05a9177-5719-47b3-94d9-60ab75e538a7")
	p2 := provider.NewProvider2(httpGetter, "https://run.mocky.io/v3/7dcc2ac7-85f0-4032-a421-f8d947e20824")
	p3 := provider.NewProvider3(httpGetter, "https://run.mocky.io/v3/377b087e-56bf-4a3f-a2f8-d662b4782705")
	tcmb := provider.NewTcmb(httpGetter, "https://www.tcmb.gov.tr/kurlar/today.xml")

	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Mount("/", currencyRoute{dataStore: dataStore}.Routes())

	syncer := sync.NewSync(dataStore, []provider.Provider{
		p1,
		p2,
		p3,
		tcmb,
	})
	go syncer.StartPuller()

	log.Fatal(http.ListenAndServe(":2626", r))
}
